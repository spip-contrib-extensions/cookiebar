<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/cookiebar.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Configuration de cookiebar',
	'cookiebar_titre' => 'cookiebar',

	// L
	'label_accept_button' => 'Afficher le bouton',
	'label_accept_button_false' => 'non',
	'label_accept_button_true' => 'oui',
	'label_accept_txt' => 'Personnaliser le texte du bouton (facultatif)',
	'label_accept_txt_current' => 'Intitulé du bouton par défaut',
	'label_decline_button' => 'Afficher le bouton',
	'label_decline_button_false' => 'non',
	'label_decline_button_true' => 'oui',
	'label_decline_txt' => 'Personnaliser le texte du bouton (facultatif)',
	'label_decline_txt_current' => 'Intitulé du bouton par défaut',
	'label_effet' => 'Disparation de la barre',
	'label_effet_fade' => 'Fondu (fade)',
	'label_effet_hide' => 'Caché (hide)',
	'label_effet_slide' => 'Glissé (slide)',
	'label_message_txt' => 'Personnaliser le message (facultatif) syntaxe &lt;multi&gt;&lt;/multi&gt; accepté',
	'label_message_txt_current' => 'Message par défaut',
	'label_policy_button' => 'Afficher le bouton',
	'label_policy_button_false' => 'non',
	'label_policy_button_true' => 'oui',
	'label_policy_txt' => 'Personnaliser le texte du bouton (facultatif)',
	'label_policy_txt_current' => 'Intitulé du bouton par défaut',
	'label_policy_url' => 'Si oui, adresse de la page contenant la politique de confidentialités',
	'label_position' => 'Position de la barre',
	'label_position_fixed' => 'Haut de page (fixed)',
	'label_position_fixed-bottom' => 'Bas de page (fixed)',
	'label_position_top' => 'Haut de page',
	'label_section_accept' => 'Bouton d’accord des cookies',
	'label_section_decline' => 'Bouton de refus de cookies',
	'label_section_graphisme' => 'Aspect graphique',
	'label_section_message' => 'Message',
	'label_section_policy' => 'Bouton de lien vers la politique de confidentialité',

	// M
	'message_accepttext' => 'J’accepte',
	'message_declinetext' => 'Désactiver les cookies',
	'message_message' => 'En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de cookies de suivi et de préférences',
	'message_policytext' => 'Politique de confidentialité',

	// T
	'titre_page_configurer_cookiebar' => 'Configuration de cookiebar'
);
