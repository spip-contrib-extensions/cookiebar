<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cookiebar?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'message_accepttext' => 'أوافق',
	'message_declinetext' => 'تعطيل ملفات تعريف الارتباط',
	'message_message' => 'يتضمن استمرارك في تصفح هذا الموقع موافقتك على استخدام ملفات تعريف الارتباط التي تمكننا من إعداد الإحصائيات بشأن الزيارات للموقع. ',
	'message_policytext' => 'سياسة الخصوصية'
);
