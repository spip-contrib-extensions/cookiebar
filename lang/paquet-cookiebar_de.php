<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-cookiebar?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cookiebar_description' => 'Holt die Erlaubnis zum Setzen von Cookies ein.',
	'cookiebar_nom' => 'Cookiebar',
	'cookiebar_slogan' => 'Blendet Hinweis auf Cookies ein'
);
