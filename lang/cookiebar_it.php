<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cookiebar?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Configurazione della barra dei cookies',
	'cookiebar_titre' => 'Cookiebar',

	// L
	'label_accept_button' => 'Visualizzazione del pulsante',
	'label_accept_button_false' => 'no',
	'label_accept_button_true' => 'si',
	'label_accept_txt' => 'Personalizza il testo del pulsante (facoltativo)',
	'label_accept_txt_current' => 'Etichetta di default',
	'label_decline_button' => 'Visualizzazione del pulsante',
	'label_decline_button_false' => 'no',
	'label_decline_button_true' => 'si',
	'label_decline_txt' => 'Personalizza il testo del pulsante (facoltativo)',
	'label_decline_txt_current' => 'Etichetta di default',
	'label_effet' => 'Scomparsa della barra',
	'label_effet_fade' => 'Dissolvenza',
	'label_effet_hide' => 'Nascosto',
	'label_effet_slide' => 'Scivolamento (diapositiva)',
	'label_message_txt' => 'Personalizza il messaggio (facoltativo)
viene accettata la sintassi &lt;multi&gt;&lt;/multi&gt;',
	'label_message_txt_current' => 'Messaggio predefinito',
	'label_policy_button' => 'Visualizzazione del pulsante',
	'label_policy_button_false' => 'no',
	'label_policy_button_true' => 'si',
	'label_policy_txt' => 'Personalizza il testo del pulsante (facoltativo)',
	'label_policy_txt_current' => 'Etichetta di default',
	'label_policy_url' => 'Se SI, indirizzo della pagina contenente l’informativa sulla privacy',
	'label_position' => 'Posizione della barra',
	'label_position_fixed' => 'In testa alla pagina (fissa)',
	'label_position_fixed-bottom' => 'In fondo alla pagina (fissa)',
	'label_position_top' => 'In testa alla pagina',
	'label_section_accept' => 'Pulsante per l’accettazione dei cookies',
	'label_section_decline' => 'Pulsante di rifiuto dei cookies',
	'label_section_graphisme' => 'Aspetto grafico',
	'label_section_message' => 'Messaggio',
	'label_section_policy' => 'Pulsante di collegamento all’informativa sulla privacy',

	// M
	'message_accepttext' => 'Accetto',
	'message_declinetext' => 'Disattiva i cookies',
	'message_message' => 'Proseguendo la visita a questo sito, accetti l’utilizzo dei cookie e delle preferenze di tracciamento',
	'message_policytext' => 'Informativa sulla privacy',

	// T
	'titre_page_configurer_cookiebar' => 'Configura Cookiebar'
);
