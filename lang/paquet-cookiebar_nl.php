<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-cookiebar?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cookiebar_description' => 'Vraag het akkoord van de gebruiker voor het gebruik van cookies.',
	'cookiebar_nom' => 'cookiebar',
	'cookiebar_slogan' => 'Geef een waarschuwing over het gebruik van cookies weer'
);
