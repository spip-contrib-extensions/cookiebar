<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/cookiebar.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cookiebar_description' => 'Demande le consentement à l’internaute à l’utilisation des cookies.',
	'cookiebar_nom' => 'cookiebar',
	'cookiebar_slogan' => 'Affiche un avertissement à l’utilisation des cookies'
);
